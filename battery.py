'''
Author: Derek Aherne
Date: 2020-11-05
contact: derek.aherne@sse.com

Description:
A temporal arbitrage trading model. The default CBC optimiser in PuLP is implemented for each day of a given time series in a df

'''

import pulp
import time
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import logging
logger = logging.getLogger(__name__)

class Battery():
    '''
    The battery class sets up the optimisers objective function and constraints. 
    The model is solved and its ouput returned.
    
    parameters intitialised:
    param: time_horizon - the length of the time periods (eg 48 for daily half hours)
    param: max_discharge_power_capacity - the max discharge power of the battery (MW)
    param: max_charge_power_capacity - the max charge power of the battery (MW)
    
    '''
    
    def __init__(self,
                 time_horizon,
                 max_discharge_power_capacity,
                 max_charge_power_capacity):
        
        '''
        The charge and discharge flows and optimisation horizon - the decision variables
        '''
        
        self.time_horizon = time_horizon

        self.charge = \
            pulp.LpVariable.dicts(
                "charging_power",
                ('c_t_' + str(i) for i in range(0,time_horizon)),
                lowBound=0, upBound=max_charge_power_capacity,
                cat='Continuous')

        self.discharge = \
            pulp.LpVariable.dicts(
                "discharging_power",
                ('d_t_' + str(i) for i in range(0,time_horizon)),
                lowBound=0, upBound=max_discharge_power_capacity,
                cat='Continuous')

    def set_objective(self, prices):
        
        '''
        The objective function. Calculates the profit for each time horizon  
        
        param: a list of prices
        
        '''
        try:
            assert len(prices) == self.time_horizon
        except:
            logging.warning('Error: need one price for each time step')
        
        #Instantiate the model
        self.model = pulp.LpProblem("Energy arbitrage", pulp.LpMaximize)

        self.model += \
            pulp.LpAffineExpression(
                [(self.charge['c_t_' + str(i)],
                  -1*prices[i]) for i in range(0,self.time_horizon)]) +\
            pulp.LpAffineExpression(
                [(self.discharge['d_t_' + str(i)],
                  prices[i]) for i in range(0,self.time_horizon)])
        
    def add_storage_constraints(self,
                                efficiency,
                                min_capacity,
                                discharge_energy_capacity,
                                initial_level):
        '''
        Adding the constraints
        - battery cannot have less than zero capacity
        - battery cannot have more than max capacity
        
        The battert starts with an initial charge. This can be carried over 
        from the previous day. The efficiency is factored in the charging period.
        
        param: efficiency - the round trip efficiency of the battery
        param: min_capacity - min capacity the battery
        param: discharge_energy_capacity - charging power
        param: initial_level - a starting charge level
        
        '''
        # 1
        # charging capacities
        for hour_of_sim in range(1,self.time_horizon+1):     
            self.model += \
            initial_level \
            + pulp.LpAffineExpression(
                [(self.charge['c_t_' + str(i)], efficiency) #efficiency
                 for i in range(0,hour_of_sim)]) \
            - pulp.lpSum(
                self.discharge[index]
                for index in('d_t_' + str(i)
                             for i in range(0,hour_of_sim)))\
            >= min_capacity
            
        #2
        #discharge energy capacity
        for hour_of_sim in range(1,self.time_horizon+1):
            self.model += \
            initial_level \
            + pulp.LpAffineExpression(
                [(self.charge['c_t_' + str(i)], efficiency)
                 for i in range(0,hour_of_sim)]) \
            - pulp.lpSum(
                self.discharge[index]
                for index in ('d_t_' + str(i)
                              for i in range(0,hour_of_sim)))\
            <= discharge_energy_capacity
            
    def add_throughput_constraints(self, max_daily_discharged_throughput):
        '''
        Control the throughput of the battery. This limits the daily cycle rate of the battery
        
        param: max_daily_discharged_throughput - the maximum energy flow through the batter (ie cycles)
        '''
        
        self.model += \
        pulp.lpSum(
            self.discharge[index] for index in (
                'd_t_' + str(i) for i in range(0,self.time_horizon))) \
        <= max_daily_discharged_throughput
        
        
    def solve_model(self):
        '''
        Solve the optimization problem
        '''
        self.model.solve()
        if pulp.LpStatus[self.model.status] != 'Optimal':
            logging.warning('Warning: ' + pulp.LpStatus[self.model.status])
            
    def collect_output(self):  
        '''
        Collect hourly charging and discharging slots
        '''
        hourly_charges =\
            np.array(
                [self.charge[index].varValue for
                 index in ('c_t_' + str(i) for i in range(0,self.time_horizon))])
        hourly_discharges =\
            np.array(
                [self.discharge[index].varValue for
                 index in ('d_t_' + str(i) for i in range(0,self.time_horizon))])

        return hourly_charges, hourly_discharges

    

def simulate_battery(initial_level,
                     price_data,
                     price_data_day,
                     price_data_shape,
                     max_discharge_power_capacity,
                     max_charge_power_capacity,
                     discharge_energy_capacity,
                     efficiency,
                     max_daily_discharged_throughput,
                     time_horizon,
                     plot=False):
    '''
    The battery simulator instaniates a battery object.
    For each day in the provided df the battery object
    solves the objective function to return a complet 
    dictionay containing hourly discharges and charges 
    ,associated profits, intial charge levels and a daily plot (optional)
    
    param: initial_level - starting charge level
    param: price_data - a df containig your data
    param: price_data_day - the name of the 'day' column to loop through 
    param: price_data_shape - the input price shape in the df
    param: max_discharge_power_capacity - power
    param: max_charge_power_capacity - power
    param: discharge_energy_capacity - energy
    param: efficiency 
    param: max_daily_discharged_throughput - cycles
    param: time_horizon - should be the length of the daily price shape
    param: plot - set to True if you want daily plots
    
    
    '''
    
    #Track simulation time
    tic = time.time()
    all_data = {}
    
    #Initialize output variables
    all_profit = []
    
    #Set up decision variables for optimization by
    #instantiating the Battery class
    battery = Battery(
        time_horizon=time_horizon,
        max_discharge_power_capacity=max_discharge_power_capacity,
        max_charge_power_capacity=max_charge_power_capacity)
    
    #############################################
    #Run the optimization for each day of the year.
    #############################################
    #print(price_data[price_data_day].unique())
    for i, day in enumerate(price_data[price_data_day].unique()):
        df_sub = price_data[price_data[price_data_day] == day].reset_index()        
        prices = df_sub[price_data_shape].values   
        #logging.warning('simulating {}'.format(day))
        
        #############################################
        ### Select data and simulate daily operation
        #############################################
        
        #Create model and objective
        try:
            battery.set_objective(prices)


            #Set storage constraints
            battery.add_storage_constraints(
                    efficiency=efficiency,
                    min_capacity=0,
                    discharge_energy_capacity=discharge_energy_capacity,
                    initial_level=initial_level)

            #Set maximum discharge throughput constraint
            battery.add_throughput_constraints(
                    max_daily_discharged_throughput=
                    max_daily_discharged_throughput)

                #Solve the optimization problem and collect output
            battery.solve_model()
            hourly_charges, hourly_discharges = battery.collect_output()
            profit = (sum(np.array(hourly_discharges*prices)) - sum(np.array(hourly_charges*prices)))
        except:
            logging.warning('error with {}'.format(day))
            continue
        #############################################
        #############################################
        
        #Collect daily discharge throughput
        daily_discharge_throughput = sum(hourly_discharges)
        net_hourly_activity = (hourly_charges*efficiency) - hourly_discharges
        #Cumulative changes in energy over time
        cumulative_hourly_activity = np.cumsum(net_hourly_activity)
        #Add the baseline for hourly state of energy during the next
        #time step (t2)
        state_of_energy_from_t2 = initial_level \
        + cumulative_hourly_activity
        
        
        #############################################
        ### create plots and update dictionary
        #############################################
        if plot:
            print(hourly_charges)
            print(hourly_discharges)
            fig = plt.figure(figsize=(15,5))
            plt.plot()
            plt.plot(prices, 'green',label='price')
            ax2 = plt.twinx()    
            ax2.plot(hourly_discharges,'red',ms=7,label='Export')
            ax2.plot(hourly_charges,'blue',ms=7,label='Import')
            plt.grid(True)
            plt.legend(loc="upper left")
            plt.show()
            #break
            all_data.update({day:{'initial_level':initial_level,'throughput':daily_discharge_throughput,'prices':str(prices).strip('[]'),'charges':str(hourly_charges).strip('[]'),'discharges':str(hourly_discharges).strip('[]'),'sim days':i,'profit':profit}})
        else:
            all_data.update({day:{'initial_level':initial_level,'throughput':daily_discharge_throughput,'prices':str(prices).strip('[]'),'charges':str(hourly_charges).strip('[]'),'discharges':str(hourly_discharges).strip('[]'),'sim days':i,'profit':profit}})
        
        
        #############################################
        ### Set up the next day
        #############################################
        initial_level = int(state_of_energy_from_t2[-1])

    toc = time.time()
    logging.info('Total simulation time: ' + str(toc-tic) + ' seconds')

    return all_data


def solve_one_day(prices,
                  max_discharge_power_capacity,
                  max_charge_power_capacity,
                  efficiency,
                  discharge_energy_capacity,
                  initial_level,
                  max_daily_discharged_throughput,
                 plot=True): 
    '''
    Use the battery class to simulate a battery over a single list of prices
    
    --param--
    prices: input prices, a list of floats
    max_discharge_power_capacity: charging power in MW
    max_charge_power_capacity: charging power in MW
    efficiency: round trip efficiency
    discharge_energy_capacity: Energy in MWh
    initial_level: startinig SOC
    max_daily_discharged_throughput: how much throughput can the battery take, ie cycles
    
    --return--
    hourly_charges: points and quantity of charge
    hourly_discharges: points and quantity of discharge
    residual_level: capacity remaining at the end of the day
    daily_discharge_throughput: throughput for the day
    '''
    #############################################
    ### Select data and simulate daily operation
    #############################################
    all_data = {}
    #Initialize output variables
    all_profit = []
    battery = Battery(
        time_horizon=len(prices),
        max_discharge_power_capacity=max_discharge_power_capacity,
        max_charge_power_capacity=max_charge_power_capacity)

    battery.set_objective(prices)


    #Set storage constraints
    battery.add_storage_constraints(
            efficiency=efficiency,
            min_capacity=0,
            discharge_energy_capacity=discharge_energy_capacity,
            initial_level=initial_level)

    #Set maximum discharge throughput constraint
    battery.add_throughput_constraints(
            max_daily_discharged_throughput=
            max_daily_discharged_throughput)

    #Solve the optimization problem and collect output
    battery.solve_model()
    hourly_charges, hourly_discharges = battery.collect_output()
    profit = (sum(np.array(hourly_discharges*prices)) - sum(np.array(hourly_charges*prices)))
 
    #############################################
    #############################################

    #Collect daily discharge throughput
    daily_discharge_throughput = sum(hourly_discharges)
    net_hourly_activity = (hourly_charges*efficiency) - hourly_discharges
    #Cumulative changes in energy over time
    cumulative_hourly_activity = np.cumsum(net_hourly_activity)
    #Add the baseline for hourly state of energy during the next
    #time step (t2)
    state_of_energy_from_t2 = initial_level + cumulative_hourly_activity


    #############################################
    ### create plots and update dictionary
    #############################################
    if plot:
        fig = plt.figure(figsize=(15,5))
        plt.plot()
        plt.plot(prices, 'green',label='price')
        ax2 = plt.twinx()    
        ax2.plot(hourly_discharges,'red',ms=7,label='Export')
        ax2.plot(hourly_charges,'blue',ms=7,label='Import')
        plt.grid(True)
        plt.legend(loc="upper left")
        plt.show()
   

    #############################################
    ### Set up the next day
    #############################################
    residual_level = int(state_of_energy_from_t2[-1])

    return hourly_charges,hourly_discharges,residual_level,daily_discharge_throughput,net_hourly_activity,state_of_energy_from_t2    