import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import sys
sys.path.append('C:\\Users\\Derek\\Documents\\sse_projects\\one_day_battery_arbitrage\\apps')
import navbar
#sys.path.append('C:\\Users\\Derek\\Documents\\sse_projects\\one_day_battery_arbitrage\\')
from app import app,server

card = dbc.Container(dbc.Card(
    [
        dbc.CardBody(
            [
                html.H4("Problem Definition", className="card-title"),
                dcc.Markdown('''
                             ***
                             Given a time series of market prices optimise the charging and discharge strategy of a grid connected battery that will  maximise profit. The model should be constrained to the the phyhsical constraints of a battery; power, energy, round-trip efficiencies and depth of discharge. The model should be both deterministic and stochastic - allowing historic but also robust analysis.                            
                             '''),
                html.H4("Modelling Approach", className="card-title"),
                dcc.Markdown('***'),
                html.P(
                    "The underlying model uses linear programming (defaults to CBC) and a DSP filtering technique.\
                    The model optimises across a historical data set or a Monte Carlo simulation."
                ),
                html.P("The model takes a price shape as an input to optimise. \
                       The output is applied to an actual price shape \
                       to determine a profit for each day in a time series \
                       "),
                dcc.Markdown('###### An example of one iteration:'),
                html.Div(html.Img(src=app.get_asset_url('model_output_example.png'),style={'height':'250px'})),
                dcc.Markdown('''
                            ***
                            #### Model Definition
                            ***
                            ###### Objective Function:

                            '''
                            ),
                html.Img(src=app.get_asset_url('ob_f_2.png')),
                dcc.Markdown('''
                                where: P is the total profit, \
                                t is the time step,pt is the energy \
                                price at that time step, ct is the charging flow (MWh), \
                                dt is the dischraging flow (MWh)
                                
                            '''),
                dcc.Markdown('''
                ###### Constraints:
                '''
                ),
                html.Img(src=app.get_asset_url('c_1.2.png')),#,style={'height':'40%', 'width':'40%'}),

                dcc.Markdown('''

                where: 

                n is the round-trip efficiency of the battery.
    
                '''
                            ),
                html.Img(src=app.get_asset_url('c_2.1.png')),
                dcc.Markdown('''

                ###### Assumptions:

                Charging Power = Discharging Power, Depth of Discharge = 100%
                ***
                '''),
                html.H4("Monte Carlo Simulations", className="card-title"),
                dcc.Markdown('***'),
                html.P(
                    "The Monte Carlo Model assumes a skewed normal distribution \
                    with varying shapes at each time period. Samples are subsequently drawn from a skewed \
                    normal distribution, where the parameters are drawn from fitting a skewed normal \
                    ditribution to observed historic prices. Therefore a different parameterised distribution is used for each time step. \
                    ",
                    className="card-text",),
                dcc.Markdown('###### An example of a 9am distribution:'),
                dbc.Row([html.Img(src=app.get_asset_url('price_shp_9.png')),html.Img(src=app.get_asset_url('gen_shp_9.png'))],align="center"),
                 html.P(
                    "Subsequently a price shape is generated, from a set of observed parameters, within each iteration. The shape is passed through a Savitzky-Golay Filter to smooth a more realistic shape",
                    className="card-text",),
                dcc.Markdown('###### An example of filtering:'),                dbc.Row([html.Img(src=app.get_asset_url('sim_day.png')),html.Img(src=app.get_asset_url('filter_day.png'))],align="center"),
                dcc.Markdown('###### Comparing full distributions:'), 
                dbc.Row([html.Img(src=app.get_asset_url('sim_act_price.png'))],align="center")
                
            ]
        ),
    ]
)
)


layout = html.Div(
    children = [navbar.logo,card,navbar.footer])

#if __name__ == "__main__":
    #app.run_server(debug=False)
     