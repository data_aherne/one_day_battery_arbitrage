import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc

from app import app , server
##############################
### RADIO
#############################
model_picker = dcc.RadioItems(
        id='Model Data',
        options=[{'label': 'Monte Carlo  ', 'value': 1},{'label': 'Historical Data  ', 'value': 0}] ,
        value='1'
)

##############################
#### CHECK LIST
##############################

duration_list = dbc.RadioItems(
            options=[
                {"label": "1 Hour", "value": 1},
                {"label": "1.5 Hour", "value": 1.5},
                {"label": "2 Hour", "value": 2}
            ],
            value=1,
            id="duration-input",
            inline=False,
)

cycle_list =dbc.RadioItems(
            options=[
                {"label": "1/day", "value": 1},
                {"label": "1.5/day", "value": 1.5},
                {"label": "2/day", "value": 2}
            ],
            value=1,
            id="cycle-input",
            inline=False,
)
 

##############################
### SLIDERS
##############################
yr_slider = dcc.RangeSlider(
    id="year-slider",
    min=2016,
    max=2020,
    #step=1,
    marks={
        2016: '2016',
        2017: '2017',
        2018: '2018',
        2019: '2019',
        2020: '2020'
    },
    value=[2016,2020]
)  

pwr_slider = dcc.Slider(
    id="pwr-slider",
    min=50,
    max=100,
    step=1,
    marks={
        50: '50 MW',
        75: '75 MW',
        100: '100 MW',
        
    },
    value=50,
    included=False,
    updatemode='drag'
    
)

sim_slider = dcc.Slider(
    id="sim-slider",
    min=1000,
    max=50000,
    step=5000,
    marks={
        1000: '1000',
        50000: '50,000',
    },
    value=1000,
    included=False
    
)

efficiency_slider = dcc.Slider(
    id="eff-slider",
    min=70,
    max=100,
    step=1,
    marks={
        70: '70%',
        85: '85%',
        100: '100%'
    },
    value=85,
    included=False,
    updatemode='drag'
)  

depth_dis_slider = dcc.Slider(
    id="dep-slider",
    min=90,
    max=100,
    step=1,
    marks={
        90: '90%',
        95: '95%',
        100: '100%'
    },
    value=100,
    included=False,
    disabled=True,
    updatemode='drag'

)  

soc_slider = dcc.Slider(
    id="soc-slider",
    min=0,
    max=100,
    step=1,
    marks={
        0: '0%',
        50: '50%',
        100: '100%'
    },
    value=0,
    included=False,
    updatemode='drag'
)  