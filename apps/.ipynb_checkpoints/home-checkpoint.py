import pandas as pd
import plotly.graph_objs as go
import navbar
import battery
import numpy as np
from app import app,server
from datetime import datetime
import dash_html_components as html
import dash_bootstrap_components as dbc
import components as com
from dash.dependencies import Input, Output , State
import dash_core_components as dcc
import dash_table
import time
import dash_daq as daq
from plotly.subplots import make_subplots

##############################
######### variables ##########
##############################
times = ['00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00'\
        ,'07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00'\
        ,'15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30','00:00'] 

morning = times[0:16]
afternoon = times[16:32]
evening = times[32:]

try:
    clicks
except NameError:
    clicks = 0
print('clicks is now ....',clicks)

##############################
########## styling ###########
##############################
sse_blue = '#004687'
led_color = '#78C2AD'
led_size = 15
drk_thm_grey = "#adb5bd"
butt_style = style = {'background-color':sse_blue,'color':'white', 'border': sse_blue}
sld_sub_text_style = {'color':'#A9A9A9','margin-bottom': 10,'margin-bottom': 10}

#results table

#############################
#########   data  ###########
#############################
#mock input
price = np.random.randint(50, size=48)
df = pd.DataFrame()
df['Times']=times
df['Prices']=price

#############################
##### component builder #####
#############################

#graph build
def build_line_bar(title,line_name,bar_name,bar_name_2,x,y_scatter,y_bar,y_bar_2):
    
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig.add_trace(go.Scatter(
                        y=y_scatter,
                        x=x,
                        mode="lines+markers",
                        marker={"size": 8},
                        name=line_name,
                        line_color=sse_blue,
                        #secondary_y=False,
                        #fill='tozeroy',
                        #opacity=0.7
                    )
    )
    fig.add_trace(go.Scatter(
                name=bar_name_2,
                x=x,
                y=y_bar,
                opacity=0.5,
                #color='#004687'
                yaxis='y2',
                fill='tozeroy'
                #marker_color='#adb5bd'
            )
    )
    fig.add_trace(go.Scatter(
                name=bar_name,
                x=x,
                y=y_bar_2,
                opacity=0.5,
                #color='#004687'
                yaxis='y2',
                marker_color='#adb5bd',
                fill='tozeroy'
            )
    )
    fig.update_layout(
                title=title,
                #plot_bgcolor= 'rgb(203, 213, 232)',
                #paper_bgcolor= '#adb5bd',
                height=450,
                template = 'plotly_white',
                legend=dict(
                orientation="h",
                yanchor="bottom",
                y=1.02,
                xanchor="right",
                x=1,
                ))
    fig.update_yaxes(title_text=line_name, secondary_y=False)
    fig.update_yaxes(title_text=bar_name, secondary_y=True)
    return fig



# LED builds
def led_builder(id,label,value):
    led = daq.LEDDisplay(
      id=id,
      size = led_size,
      color = led_color,
      label = label,
      value=value,
      )  
    return led

led_put = led_builder('led_put','Throughput (MWh)','150.00')
led_cyc = led_builder('led_cyc','Cycles','1.50')
led_dis = led_builder('led_dis','Discharge Revenue','15000.00')
led_soc = led_builder('led_soc','State Of Charge','12.00')
led_profit = led_builder('led_profit','Net Revenue','15660.00')
led_cost = led_builder('led_cost','Charge Cost','15880.00')


#Button builds
button = html.Div(
    [
        dbc.Button("Run Model", id="run-button", className="mr-2",block=False,size="md",outline=False,n_clicks=0, style=butt_style),
        dbc.Button("Clear Data", id="clear-button", className="mr-2",block=False,size="md",outline=True,n_clicks=0, color='primary'),
        dbc.Button("Generate Data", id="gen-button", className="mr-2",block=False,size="md",outline=True,n_clicks=0, color='primary'),
        
    ],style={'verticalAlign': 'middle', 'width': '200px','display': 'inline'}
)

#cards and forms
durations = dbc.FormGroup(
    [
        dbc.Label("Duration"),
        com.duration_list
    ]
)


battery_card_2 = dbc.Container(
  
        dbc.FormGroup(
            [html.H5("Battery Parameters:", className="card-title"),
             dbc.Row([
                 dbc.Col([dbc.Label("Cycles per day"),com.cycle_list],md=2),
                 dbc.Col(durations,md=2),
                 dbc.Col([dbc.Label("SOC"),com.soc_slider,html.Div(id='updatemode-soc-container', style=sld_sub_text_style)]),
                 dbc.Col([dbc.Label("Power"),com.pwr_slider,html.Div(id='updatemode-pwr-container', style=sld_sub_text_style)]),
                 dbc.Col([dbc.Label("Efficiency"),com.efficiency_slider,html.Div(id='updatemode-eff-container', style=sld_sub_text_style)]),
                 dbc.Col([dbc.Label("Depth of discharge"),com.depth_dis_slider,html.Div(id='updatemode-dep-container', style=sld_sub_text_style)])
             ])         
            ]
        )
    
)


# tables 

def table_builder(title,id,df,editable,height):
    table = dbc.Container([
    html.H6(title, className="card-title"),
    dash_table.DataTable(
        id=id,
        fixed_rows={'headers': True},
        style_table={'height': height} , # defaults to 500
        style_header={'backgroundColor': sse_blue,'color': 'white'},
        style_data = {'whiteSpace': 'normal','height': 'auto','lineHeight': '15px'},
        style_cell={'textAlign': 'center','minWidth': '18px', 'width': '18px', 'maxWidth': '18px',},
        columns =[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict('records'),
        editable=editable
    )]) 
    return table

def empty_table(times):
    res_df = pd.DataFrame(columns=times)
    res_df.insert(0,' ',['Price','SOC','Buy','Sell','Revenue'])
    return res_df

res_morn = empty_table(morning)
res_aft = empty_table(afternoon)
res_eve = empty_table(evening)

table = table_builder("Data Input",'table-editing-simple',df,True,360)
results_table = table_builder("Morning Strategy",'morning_table',res_morn,False,'auto')
results_afternoon = table_builder("Afternoon Strategy",'afternoon_table',res_aft,False,'auto')
results_evening = table_builder("Evening Strategy",'evening_table',res_eve,False,'auto')

# data input card
data_input = dbc.Container(
        dbc.FormGroup(
            [html.H4("Input Data", className="card-title"),
             dbc.Row(
                dbc.Col([table],md=2)
                 )
            ]
            
        )
)

#############################
######### call backs ########
#############################
@app.callback(Output('updatemode-pwr-container', 'children'),
              [Input('pwr-slider', 'value')])
def display_value(value):
    return '{} MW'.format(value)

@app.callback(Output('updatemode-eff-container', 'children'),
              [Input('eff-slider', 'value')])
def display_value(value):
    return '{}%'.format(value)

@app.callback(Output('updatemode-soc-container', 'children'),
              [Input('soc-slider', 'value')])
def display_value(value):
    return '{}%'.format(value)

@app.callback(Output('updatemode-dep-container', 'children'),
              [Input('dep-slider', 'value')])
def display_value(value):
    return '{}%'.format(value)


###########################
#### model call back   ####
###########################

@app.callback(
    [
    Output('prices_graph', 'figure'),
    Output('led_put', 'value'),
    Output('led_cost', 'value'),
    Output('led_dis', 'value'),
    Output('led_profit', 'value'),
    Output('led_soc', 'value'),
    Output('led_cyc', 'value'),
        Output('morning_table', 'data'),
        Output('morning_table', 'columns'),
        Output('afternoon_table', 'data'),
        Output('afternoon_table', 'columns'),
        Output('evening_table', 'data'),
        Output('evening_table', 'columns'),
    ],
    
    [
    
        Input("cycle-input","value"),
        Input("duration-input","value"),
        Input("pwr-slider","value"),
        Input("eff-slider","value"),
        Input("soc-slider","value"),
        Input('table-editing-simple', 'data'),
        Input("run-button", "n_clicks")
        
    ],State('prices_graph', 'figure')
)

def run_model(cycle,dur,pwr,eff,soc,rows,n_clicks,fig):
    global clicks
    prc_df = pd.DataFrame.from_records(rows)
    y=prc_df.Prices.astype('float')

    if n_clicks > clicks :
        clicks = n_clicks
        print('clicks: ',clicks)
        duration = dur #hrs
        power = pwr #MW
        energy = duration * power #MWh
        cycle = cycle #per day
        soc = soc/100
        max_discharge_power_capacity = power/2
        print('power: {}'.format(max_discharge_power_capacity))
        max_charge_power_capacity = max_discharge_power_capacity 
        discharge_energy_capacity = energy 
        print('energy: {}'.format(discharge_energy_capacity))
        efficiency = eff/100 #unitless
        print('efficiency: {}'.format(efficiency))
        max_daily_discharged_throughput = cycle * energy 
        print('throughput: {}'.format(max_daily_discharged_throughput))
        initial_level = soc*energy
        print('soc: {}'.format(initial_level))
        
        
        m = battery.solve_one_day(y,
                  max_discharge_power_capacity,
                  max_charge_power_capacity,
                  efficiency,
                  discharge_energy_capacity,
                  initial_level,
                  max_daily_discharged_throughput,
                  plot=False) 
        fig = build_line_bar("Price Shape and State Of Charge","Price","SOC",'Charge',times,y,m[4].tolist(),m[5].tolist())
        tput = m[3]
        cost = (y*m[0]).sum()
        ben = (y*m[1]).sum()
        rev = ben-cost
        residual = m[5]
        residual=residual[-1]
        cyc = tput/discharge_energy_capacity

        morning_prc = y[0:16].copy()
        afternoon_prc = y[16:32].copy()
        evening_prc = y[32:].copy()
        
        
        #rebuild tables
        for i,time in enumerate(morning):
            index=i
            res_morn.at[0,time] = round(y[0:16][index],2)
            res_morn.at[1,time] = round(float(m[5][0:16][index]),2)
            res_morn.at[2,time] = round(m[0][0:16][index],2)
            res_morn.at[3,time] = round(m[1][0:16][index],2)
            res_morn.at[4,time] = round((m[0][0:16][index]*-1+m[1][0:16][index])*y[0:16][index],2)
     
        for i,time in enumerate(afternoon):
            index=i
            res_aft.at[0,time] = round(afternoon_prc.values[index],2)
            res_aft.at[1,time] = round(float(m[5][16:32][index]),2)
            res_aft.at[2,time] = round(m[0][16:32][index],2)
            res_aft.at[3,time] = round(m[1][16:32][index],2)
            res_aft.at[4,time] = round((m[0][16:32][index]*-1+m[1][16:32][index])*afternoon_prc.values[index],2)
        
        for i,time in enumerate(evening):
            index=i
            res_eve.at[0,time] = round(evening_prc.tolist()[index],2)
            res_eve.at[1,time] = round(float(m[5][32:][index]),2)
            res_eve.at[2,time] = round(m[0][32:][index],2)
            res_eve.at[3,time] = round(m[1][32:][index],2)
            res_eve.at[4,time] = round((m[0][32:][index]*-1+m[1][32:][index])*evening_prc.tolist()[index],2)
        
    else:
        #time.sleep(1)
        fig = build_line_bar("Price Shape and State Of Charge","Price","SOC",'Charge',times,y,[0 for i in range(48)],[0 for i in range(48)])
        tput = 0
        cost = 0
        ben = 0
        rev = 0
        residual = 0
        cyc=0
    #print(residual)
    return fig,'{:.2f}'.format(tput),\
                '{:.2f}'.format(cost),\
                '{:.2f}'.format(ben),\
                '{:.2f}'.format(rev),\
                '{:.2f}'.format(int(residual)),\
                '{:.2f}'.format(cyc),\
                res_morn.to_dict('records'),\
                [{"name": i, "id": i} for i in res_morn.columns],\
                res_aft.to_dict('records'),\
                [{"name": i, "id": i} for i in res_aft.columns],\
                res_eve.to_dict('records'),\
                [{"name": i, "id": i} for i in res_eve.columns],\

#############################
######### layout   ##########
#############################
layout = html.Div(children=[
                            
                            dbc.Container([
                            navbar.logo,
                            dcc.Markdown('***'),
                            dbc.Row([battery_card_2]),
                            dbc.Row([
                            
                            dbc.Col(dbc.Container([
                            #dcc.Loading(dcc.Graph(id='prices_graph')),
                            dcc.Graph(id='prices_graph'),
                            button,
                            
                                                     
                            
                            ]),md=9),dbc.Col(dbc.Container([table]),md=3),]
                            ),
                            dcc.Markdown('***'),    
                            html.H5("Results:", className="card-title"),
                            dbc.Container(dbc.Row([dbc.Col(led_cost,md=2),dbc.Col(led_dis,md=2),dbc.Col(led_profit,md=2),dbc.Col(led_soc,md=2),dbc.Col(led_put,md=2),dbc.Col(led_cyc,md=2)])),
                            dcc.Markdown('***'),
                            dbc.Row([results_table]),
                            dcc.Markdown('***'),
                            dbc.Row([results_afternoon]),
                            dcc.Markdown('***'),                               
                            dbc.Row([results_evening]),
                            dcc.Markdown('***'),
                            
                            navbar.footer])]
                 )
        


   
