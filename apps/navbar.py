import dash
import dash_table
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
#from app import app , server
from app import app,server

nav_item_home = dbc.NavItem(dbc.NavLink("home",id = "home-url", href="/apps/home"))
nav_item = dbc.NavItem(dbc.NavLink("about",id = "about-url", href="/apps/about"))
#nav_item_home = dbc.NavItem(dbc.NavLink("home", href="index.layout"))


# this example that adds a logo to the navbar brand
logo = dbc.Navbar(
    dbc.Container(
        [
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(html.Img(src=app.get_asset_url('sse_tiny.png'))),
                        dbc.Col(dbc.NavbarBrand("Temporal Arbitrage Modelling - Ex-ante Markets", className="ml-2")),
                    ],
                    align="center",
                    no_gutters=True,
                ),
            ),
            dbc.NavbarToggler(id="navbar-toggler2"),
            dbc.Collapse(
                dbc.Nav(
                    [nav_item_home,nav_item], className="ml-auto", navbar=True
                ),
                id="navbar-collapse2",
                navbar=True,
            ),
        ]
    ),
    #color="dark",
    #dark=True,
    #className="mb-5",
)

footer = dbc.Container(dbc.Row(
            [dbc.Col(html.Footer(html.P('SSE Airtricity - Energy Markets Team  © 2020')))])
                    )

