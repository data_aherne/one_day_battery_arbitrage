import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output , State
from app import app , server
from apps import about, home 
import navbar

@app.callback(
    Output(component_id="page-content",component_property='children'),
    Input(component_id="url",component_property='pathname')
    )
def display_page(pathname):
    return home.layout
    
app.layout = html.Div([dcc.Location(id="url",refresh=False),html.Div(id="page-content",children=[])])


if __name__ == "__main__":
    app.run_server(debug=True)