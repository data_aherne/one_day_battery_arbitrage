# one_day_battery_arbitrage

## Introduction
`one_day_battery_arbitrage` is a dashboard for optimising a charging and discharging strategy when applied to the ISEM (or similar) ex-ante energy market

## Built With
* [Dash](https://dash.plot.ly/) - Main server and interactive components 
* [Plotly Python](https://plot.ly/python/) - Used to create the interactive plots

## Requirements
I am running Python 3 for this applied

## How to use this app

Run this app locally by:
```
python app.py
```
Open http://localhost:8888/tree? in your browser - follow the on-screen instructions

## What does this app do



## Resources and references
* [Dash User Guide](https://dash.plot.ly/)
